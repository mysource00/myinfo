﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myInfoTest.Interface
{
    public interface IMyInfoGatewayService
    {
        Task<string> GetPersonData(string keyword, string attributes);
    }
}
