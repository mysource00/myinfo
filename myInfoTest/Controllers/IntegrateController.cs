﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using myInfoTest.Interface;
using myInfoTest.Models;
using Newtonsoft.Json.Linq;

namespace myInfoTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntegrateController : ControllerBase
    {
        private readonly IMyInfoGatewayService _myinfoGatewayService;
        public IntegrateController(
       IMyInfoGatewayService myinfoGatewayService)
        {
            _myinfoGatewayService = myinfoGatewayService;
        }
        // Test
        [HttpGet]
        public ActionResult<IEnumerable<string>> Test()
        {
            return new string[] { "Hello", "value2" };
        }

        /// <summary>
        /// Get Thirdparty address summary
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPersonData")]
        public async Task<GetPersonDataResponse> GetPersonData(
            [FromBody] GetPersonDataRequest request)
        {
            try
            {
                if (request == null || string.IsNullOrEmpty(request.keyword))
                {
                    throw new Exception("Invalid Request");
                }

                var resp = await _myinfoGatewayService.GetPersonData(request.keyword, request.attributes);

                return new GetPersonDataResponse(JObject.Parse(resp));
            }
            catch (Exception ex)
            {
                return new GetPersonDataResponse(ex);
            }
        }
    }
}