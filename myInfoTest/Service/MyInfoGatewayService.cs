﻿using myInfoTest.Interface;
using myInfoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Web;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace myInfoTest.Service
{
    public class MyInfoGatewayService : IMyInfoGatewayService
    {
        public async Task<string> GetPersonData(string keyword, string attributes)
        {
            PersonDetailResponse resp = new PersonDetailResponse();
            var _attributes = "uinfin,name,sex,race,nationality,dob,email,mobileno,regadd,housingtype,hdbtype,marital,edulevel,noa-basic,ownerprivate,cpfcontributions,cpfbalances";
            var _clientId = "STG2-MYINFO-SELF-TEST";
            var _clientSecret = "44d953c796cccebcec9bdc826852857ab412fbe2";
            var _redirectUrl = "http://localhost:3001/callback";
            var _tokenApiUrl = "https://sandbox.api.myinfo.gov.sg/com/v3/token";

            if (!string.IsNullOrEmpty(attributes))
            {
                _attributes = attributes;
            }

            try
            {
                dynamic result = null;

                var Client = new HttpClient();

                var data = new Dictionary<string, string>();
                data.Add("grant_type", "authorization_code");
                data.Add("redirect_uri", _redirectUrl);
                data.Add("code", keyword);
                data.Add("client_id", _clientId);
                data.Add("client_secret", _clientSecret);
                var content = new FormUrlEncodedContent(data);

                Client.BaseAddress = new Uri(_tokenApiUrl);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded");
                
                Client.DefaultRequestHeaders.Accept.Add(contentType);
                Client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                var accessTokenResponse = await Client.PostAsync(_tokenApiUrl, content);

                if (!accessTokenResponse.IsSuccessStatusCode)
                {
                    throw new HttpRequestException("Access token is invalid.");
                }
                var accessTokenContent = await accessTokenResponse.Content.ReadAsAsync<AccessTokenResponse>();
                if (accessTokenContent?.access_token != null && accessTokenContent.expires_in != null)
                {
                    //decode token
                    var handler = new JwtSecurityTokenHandler();
                    var accessToken = accessTokenContent.access_token;
                    //var accessToken = @"eyJ0eXAiOiJKV1QiLCJ6aXAiOiJOT05FIiwia2lkIjoiRWtnWkZDeG5taXY2T2JDZ3B4blRIRUkyK3FVPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJTOTkxMjM3NEUiLCJjdHMiOiJPQVVUSDJfU1RBVEVMRVNTX0dSQU5UIiwiYXV0aF9sZXZlbCI6MCwiYXVkaXRUcmFja2luZ0lkIjoiNmUwNzdkZjYtODJlOC00MmRjLWI3ZDItYjBmNGJlZGQ4OWZiLTMxOTIxNzMxIiwiaXNzIjoiaHR0cHM6Ly9jb25zZW50LmNsb3VkLm15aW5mby5nb3Yuc2cvY29uc2VudC9vYXV0aDIvcmVhbG1zL3Jvb3QvcmVhbG1zL215aW5mby1jb20iLCJ0b2tlbk5hbWUiOiJhY2Nlc3NfdG9rZW4iLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwiYXV0aEdyYW50SWQiOiJfdFpneDdpZFFGbzhQaXVPUWlNRENMcFczRm8iLCJhdWQiOiJteWluZm8iLCJuYmYiOjE1NzA2Nzg4OTUsImdyYW50X3R5cGUiOiJhdXRob3JpemF0aW9uX2NvZGUiLCJzY29wZSI6WyJlZHVsZXZlbCIsIm1vYmlsZW5vIiwib3duZXJwcml2YXRlIiwibmF0aW9uYWxpdHkiLCJkb2IiLCJub2EtYmFzaWMiLCJjcGZjb250cmlidXRpb25zIiwiZW1haWwiLCJzZXgiLCJob3VzaW5ndHlwZSIsImNwZmJhbGFuY2VzIiwibmFtZSIsInJlZ2FkZCIsInVpbmZpbiIsInZlaGljbGVzLnZlaGljbGVubyIsInJhY2UiLCJoZGJ0eXBlIiwibWFyaXRhbCJdLCJhdXRoX3RpbWUiOjE1NzA2Nzc1MDEsInJlYWxtIjoiL215aW5mby1jb20iLCJleHAiOjE1NzA2ODA2OTUsImlhdCI6MTU3MDY3ODg5NSwiZXhwaXJlc19pbiI6MTgwMCwianRpIjoiekxFVjhhRHl6NEFIazRmM21MSUpELTl5TXhjIn0.SQzICBWTlmSMJoEpFVcDJSF7HBKw1AVCBvLbsHKkkjqyVkJf0E1EchAKma3HA81SUWHOfGM920MtnJZqHjXtQOXI2MqdaKTGmKUdRAXCktIS6s8ytHFESyboSU8x5CXA1U_n3IzkaWglMOZRrW0pvp8Y2GALamsn42dg8qPuSw6_oWdt8iJxq_woHiTTZY-yaqbhFZw7or49N7Cy75IjN18qWcqkd1GBGD-jBl94zuCEjPL9vbCZ7rNR61tqFpgE7Sb9qDo0MEs923U6Gj-QYFfzo0Jjcw70le_z0EIGR10cJBBH4UXY9a3ume50lYhEJsj6VIxRru9-14wqC79Z-w";
                    //handler.ReadJwtToken(accessToken).Claims.Where(x => x.Type == "sub").FirstOrDefault().Value

                    var claim = handler.ReadJwtToken(accessToken).Claims.ToList();

                    var uinfinobj = claim.Where(x => x.Type == "sub").FirstOrDefault();
                    if (uinfinobj == null)
                    {
                        throw new HttpRequestException("Invalid FIN");
                    }
                    var uinfin = uinfinobj.Value;
                    using (var clientUserInfo = new HttpClient())
                    {
                        //get person data
                        var _personApiUrl = "https://sandbox.api.myinfo.gov.sg/com/v3/person";
                        var authenScheme =  "Bearer";
                        clientUserInfo.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authenScheme, accessToken);

                        var queryp = HttpUtility.ParseQueryString(string.Empty);
                        queryp["client_id"] = _clientId;
                        queryp["attributes"] =_attributes;
                        string queryString = queryp.ToString();
                        var path = string.Format("{0}/{1}/?{2}", _personApiUrl, uinfin,queryString);

                        var userInfoResponse = await clientUserInfo.GetAsync(path);
                        if (!userInfoResponse.IsSuccessStatusCode)
                        {
                            throw new HttpRequestException("User is invalid.");
                        }
                        var userInfoContent = await userInfoResponse.Content.ReadAsStringAsync();

                        return userInfoContent;

                    }

                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message);
            }

            return null;
        }
    }

    public class AccessTokenResponse
    {
        public string access_token { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
    }
}
