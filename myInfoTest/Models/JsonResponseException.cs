﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace myInfoTest.Models
{
    public class JsonResponseException : Exception
    {
        private readonly string _detail;

        public JsonResponseException(IEnumerable<Error> resultErrors)
        {
            var first = resultErrors.First();
            _detail = first.Detail;

            throw new Exception(first.Title); //false positive
        }

        public override string ToString()
        {
            return _detail;
        }
    }
}