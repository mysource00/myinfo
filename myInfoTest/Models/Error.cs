﻿using System;

namespace myInfoTest.Models
{
    /// <summary>
    /// Error element that describes the type of error
    /// http://jsonapi.org/examples/#error-objects
    /// </summary>
    public class Error
    {
        public static bool HideExceptionDetail { get; set; } = true;
        /// <summary>
        /// Error code
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Language specific error message
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Detailed error message, can be the server-side .NET stack trace
        /// </summary>
        public string Detail { get; set; }

        public Error(string title, string detail = null, string status = null)
        {
            Title = title;
            Detail = detail;
            Status = status;
        }

        public Error(Exception ex, string status = null)
        {
            if (ex != null)
            {
                Title = ex.Message;
                Detail = HideExceptionDetail ? "" : ex.ToString();
            }

            Status = status;
        }

    }
}