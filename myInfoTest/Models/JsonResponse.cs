﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace myInfoTest.Models
{
    /// <summary>
    /// JSON base response.
    /// Simple implementation based on JSON API (http://jsonapi.org/)
    /// </summary>
    public class JsonResponse<TData> 
    {
        /// <summary>
        /// Contains a collection of errors when a server-side error occurred.
        /// </summary>
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public ICollection<Error> Errors { get; set; }

        /// <summary>
        /// Data is populated when no errors are present
        /// </summary>
        public TData Data { get; set; }

        [JsonIgnore]
        public bool HasErrors => Errors != null && Errors.Count() != 0;

        /// <summary>
        /// Constructor where Data can be added or more errors can be added
        /// </summary>
        public JsonResponse()
        {
        }

        public JsonResponse(TData data)
        {
            Data = data;
        }

        /// <summary>
        /// Constructor to create a JSON exception response
        /// </summary>
        public JsonResponse(Exception exception, string errorCode = null)
        {
            AddExceptionError(exception, errorCode);
        }
        
        [Obsolete]
        public void AddError(string title, string detail = null, string status = null)
        {
            Data = default(TData); // clear data

            if (Errors == null)
                Errors = new List<Error>();  // Not thread safe

            var errors = ((List<Error>) Errors);
            errors.Add(new Error(title, detail, status));
        }

        public void AddExceptionError(Exception ex, string status = null)
        {
            Data = default(TData); // clear data
            
            if (Errors == null)
                Errors = new List<Error>();

           Errors.Add(new Error(ex));
        }
    }
}