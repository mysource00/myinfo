﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myInfoTest.Models
{
    public class GetPersonDataRequest
    {
        public string keyword { get; set; }
        public string attributes { get; set; }
    }
}
