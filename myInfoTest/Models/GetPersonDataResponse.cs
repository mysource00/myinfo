﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myInfoTest.Models
{
    public class GetPersonDataResponse : JsonResponse<object>
    {
        public GetPersonDataResponse(object data)
        {
            Data = data;
        }

        public GetPersonDataResponse(Exception exception, string errorCode = null) : base(exception, errorCode)
        {
        }
    }
}
